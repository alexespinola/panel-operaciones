var lineas = [];
$('#linea').html('').append(new Option('Seleccione una linea', ''));
$.each(storage, function (i, e) {
  if (!lineas.find(l => l == e.linea)) {
    $('#linea').append(new Option(e.linea, e.linea));
  }
  lineas.push(e.linea);
});

$('#linea').change(function () {
  var linea = $(this).val();
  var ramales = [];
  $('#ramal').html('');
  $.each(storage, function (i, e) {
    if (!ramales.find(l => l == e.ramal) && e.linea == linea) {
      $('#ramal').append(new Option(e.ramal, e.url));
    }
    ramales.push(e.ramal);
  });
});

$('#submit').click(function (e) {
  e.preventDefault();
  var url = $('#ramal').val();
  window.open(url, '_blank');
});