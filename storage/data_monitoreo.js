var storage =
[
  {
    "linea": "Sarmiento",
    "ramal": "Once - Moreno",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/mimico_itinerario/?ramal=sarmiento"
  },
  {
    "linea": "Sarmiento",
    "ramal": "Moreno - Mercedes",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/mimico_itinerario/?ramal=sarmiento_mercedes"
  },
  {
    "linea": "Sarmiento",
    "ramal": "Merlo - Lobos",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/mimico_itinerario_via_unica/?ramal=sarmiento_lobos"
  },
  {
    "linea": "Mitre",
    "ramal": "Retiro - Tigre",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/mimico_itinerario/?ramal=mitre_tigre"
  },
  {
    "linea": "Mitre",
    "ramal": "Retiro - Mitre",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=mitremitre"
  },
  {
    "linea": "Mitre",
    "ramal": "Retiro - J. L. Suárez",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/mimico_itinerario/?ramal=mitre_suarez"
  },
  {
    "linea": "Mitre",
    "ramal": "Victoria - Capilla del Señor",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/mimico_itinerario_via_unica/?ramal=mitrevictoria"
  },
  {
    "linea": "Mitre",
    "ramal": "Villa Ballester - Zárate",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/mimico_itinerario/?ramal=mitrezarate"
  },
  {
    "linea": "Mitre",
    "ramal": "Maipú - Delta",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/mimico_itinerario/?ramal=trencosta"
  },
  {
    "linea": "Roca",
    "ramal": "Plaza Constitución - La Plata",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/mimico_itinerario/?ramal=roca_laplata"
  },
  {
    "linea": "Roca",
    "ramal": "Plaza Constitución - Ezeiza",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/mimico_itinerario/?ramal=roca_ezeiza"
  },
  {
    "linea": "Roca",
    "ramal": "Plaza Constitución - Bosques-Q",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/mimico_itinerario/?ramal=roca_bosques"
  },
  {
    "linea": "Roca",
    "ramal": "Plaza Constitución - Alejandro Korn",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/mimico_itinerario/?ramal=roca_akorn"
  },
  {
    "linea": "Roca",
    "ramal": "Plaza Constitución - Cañuelas (Pza)",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/mimico_itinerario/?ramal=roca_canuelas"
  },
  {
    "linea": "Roca",
    "ramal": "Temperley - Gutiérrez",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/mimico_itinerario/?ramal=roca_gutierrez"
  },
  {
    "linea": "Roca",
    "ramal": "Plaza Constitución - Bosques-T",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/mimico_itinerario/?ramal=roca_claypole"
  },
  {
    "linea": "Roca",
    "ramal": "Temperley - Haedo",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/mimico_itinerario/?ramal=roca_haedo"
  },
  {
    "linea": "San Martin",
    "ramal": "Retiro (LSM) - Cabred",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/mimico_itinerario/?ramal=sanmartin_completo"
  },
  {
    "linea": "Belgrano sur",
    "ramal": "Buenos Aires -  M.C.G. Belgrano",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/mimico_itinerario/?ramal=belgranosur_marinos"
  },
  {
    "linea": "Belgrano sur",
    "ramal": "Buenos Aires - Gonzalez Catán",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/mimico_itinerario/?ramal=belgranosur_gcatan"
  },
  {
    "linea": "Belgrano sur",
    "ramal": "Apeadero Km 12 -  Libertad",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/mimico_itinerario/?ramal=belgranosur_km12"
  },
  {
    "linea": "Belgrano sur",
    "ramal": "Gonzalez Catán - 20 de Junio",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/mimico_itinerario/?ramal=belgranosur_junio"
  }
];