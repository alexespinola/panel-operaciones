var storage = [
  {
    "linea": "Sarmiento",
    "ramal": "Once - Moreno",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/distancias/?ramal=sarmiento"
  },
  {
    "linea": "Sarmiento",
    "ramal": "Moreno - Mercedes",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/distancias/?ramal=sarmientomercedes"
  },
  {
    "linea": "Sarmiento",
    "ramal": "Merlo - Lobos",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/distancias/?ramal=sarmientolobos"
  },
  {
    "linea": "Mitre",
    "ramal": "Retiro - Tigre",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/distancias/?ramal=mitretigre"
  },
  {
    "linea": "Mitre",
    "ramal": "Retiro - Mitre",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/distancias/?ramal=mitremitre"
  },
  {
    "linea": "Mitre",
    "ramal": "Retiro - J. L. Suárez",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/distancias/?ramal=mitresuarez"
  },
  {
    "linea": "Mitre",
    "ramal": "Victoria - Capilla del Señor",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/distancias/?ramal=mitrevictoria"
  },
  {
    "linea": "Mitre",
    "ramal": "Villa Ballester - Zárate",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/distancias/?ramal=mitrezarate"
  },
  {
    "linea": "Mitre",
    "ramal": "Maipú - Delta",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/distancias/?ramal=trencosta"
  },
  {
    "linea": "Roca",
    "ramal": "Plaza Constitución - La Plata",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/distancias/?ramal=roca_laplata"
  },
  {
    "linea": "Roca",
    "ramal": "Plaza Constitución - Ezeiza",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/distancias/?ramal=rocaezeiza"
  },
  {
    "linea": "Roca",
    "ramal": "Plaza Constitución - Bosques-Q",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/distancias/?ramal=rocabosques"
  },
  {
    "linea": "Roca",
    "ramal": "Plaza Constitución - Alejandro Korn",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/distancias/?ramal=rocaakorn"
  },
  {
    "linea": "Roca",
    "ramal": "Plaza Constitución - Cañuelas (Pza)",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/distancias/?ramal=rocacanuelas"
  },
  {
    "linea": "Roca",
    "ramal": "Temperley - Gutiérrez",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/distancias/?ramal=rocagutierrez"
  },
  {
    "linea": "Roca",
    "ramal": "Plaza Constitución - Bosques-T",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/distancias/?ramal=rocaclaypole"
  },
  {
    "linea": "Roca",
    "ramal": "Temperley - Haedo",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/distancias/?ramal=rocahaedo"
  },
  {
    "linea": "Belgrano sur",
    "ramal": "Buenos Aires -  M.C.G. Belgrano",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/distancias/?ramal=belgsurmarinos"
  },
  {
    "linea": "Belgrano sur",
    "ramal": "Buenos Aires - Gonzalez Catán",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/distancias/?ramal=belgsurgcatan"
  },
  {
    "linea": "Belgrano sur",
    "ramal": "Apeadero Km 12 -  Libertad",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/distancias/?ramal=belgranosur_km12"
  }
];