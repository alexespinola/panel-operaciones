var storage = [
  {
    "linea": "Sarmiento",
    "ramal": "Once - Moreno",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=sarmiento"
  },
  {
    "linea": "Sarmiento",
    "ramal": "Moreno - Mercedes",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=sarmientomercedes"
  },
  {
    "linea": "Sarmiento",
    "ramal": "Merlo - Lobos",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=sarmientolobos"
  },
  {
    "linea": "Mitre",
    "ramal": "Retiro - Tigre",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=mitretigre"
  },
  {
    "linea": "Mitre",
    "ramal": "Retiro - Mitre",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=mitremitre"
  },
  {
    "linea": "Mitre",
    "ramal": "Retiro - J. L. Suárez",
    "url": "https: //trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=mitresuarez"
  },
  {
    "linea": "Mitre",
    "ramal": "Victoria - Capilla del Señor",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=mitrevictoria"
  },
  {
    "linea": "Mitre",
    "ramal": "Villa Ballester - Zárate",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=mitrezarate"
  },
  {
    "linea": "Mitre",
    "ramal": "Maipú - Delta",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=trencosta"
  },
  {
    "linea": "Roca",
    "ramal": "Plaza Constitución - La Plata",
    "url": "https: //trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=rocalaplata"
  },
  {
    "linea": "Roca",
    "ramal": "Plaza Constitución - Ezeiza",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=rocaezeiza"
  },
  {
    "linea": "Roca",
    "ramal": "Plaza Constitución - Bosques-Q",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=rocabosques"
  },
  {
    "linea": "Roca",
    "ramal": "Plaza Constitución - Alejandro Korn",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=rocaakorn"
  },
  {
    "linea": "Roca",
    "ramal": "Plaza Constitución - Cañuelas (Pza)",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=rocacanuelas"
  },
  {
    "linea": "Roca",
    "ramal": "Temperley - Gutiérrez",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=rocagutierrez"
  },
  {
    "linea": "Roca",
    "ramal": "Plaza Constitución - Bosques-T",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=rocaclaypole"
  },
  {
    "linea": "Roca",
    "ramal": "Temperley - Haedo",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=rocahaedo"
  },
  {
    "linea": "San Martin",
    "ramal": "Retiro (LSM) - Cabred",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=sanmartin_completo"
  },
  {
    "linea": "Belgrano sur",
    "ramal": "Buenos Aires -  M.C.G. Belgrano",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=belgsurmarinos"
  },
  {
    "linea": "Belgrano sur",
    "ramal": "Buenos Aires - Gonzalez Catán",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=belgsurgcatan"
  },
  {
    "linea": "Belgrano sur",
    "ramal": "Apeadero Km 12 -  Libertad",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=belgranosur_km12"
  },
  {
    "linea": "Belgrano sur",
    "ramal": "Gonzalez Catán - 20 de Junio",
    "url": "https://trenes.sofse.gob.ar/v2_pg/monitoreo/velocidades/velocidades.php?ramal=belgranosur_junio"
  }
];